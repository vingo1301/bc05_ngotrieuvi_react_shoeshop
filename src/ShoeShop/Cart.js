import React, { Component } from 'react'

export default class Cart extends Component {
    contentTbody = () => {
        return this.props.cart.map((item) => {
            return <tr>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.price * item.quantity}</td>
            <td>
                <button onClick={() => this.props.handleGiamSoLuong(item)} className='btn btn-danger mx-1'>-</button>
                {item.quantity}
                <button onClick={() => this.props.handleTangSoLuong(item)} className='btn btn-success mx-1'>+</button>
                </td>
            <td>
                <img style={{width:"80px"}} src={item.image} alt="" />
            </td>
            </tr>
        })
    }
  render() {
    return (
      <div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                {this.contentTbody()}
            </tbody>
        </table>
      </div>
    )
  }
}
