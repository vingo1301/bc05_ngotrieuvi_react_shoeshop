import React, { Component } from 'react'

export default class  extends Component {
  render() {
    let {image,name} = this.props.data;
    return (
        <div className='col-3 p-1'>
        <div className="card text-left h-100">
            <img className="card-img-top" src={image} alt />
            <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <div className='wrapButton'>
            <button onClick={() => this.props.handleAddToCart(this.props.data)} className='btn btn-success mx-4'>Buy</button>
            <button onClick={() => this.props.handleChangDetail(this.props.data)} className='btn btn-danger'>Detail</button>
            </div>
            </div>
        </div>
        </div>
    )
  }
}
