import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './dataShoe'
import DetailShoe from './DetailShoe'
import ListShoe from './ListShoe'

export default class ShoeShop extends Component {
    state = {
        shoeArr : dataShoe,
        detail : dataShoe[1],
        cart : [],
    }
    handleChangDetail = (value) => {
        this.setState({
            detail: value,
        })
    }
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart];
        
        let index = this.state.cart.findIndex((item) => {
            return item.id == shoe.id;
        })
        if(index == -1){
            // khong tim thay
            let cartItem = {...shoe,quantity: 1};
            cloneCart.push(cartItem);
        }
        else{
            // tim thay
            cloneCart[index].quantity++;
        }
        
        this.setState({
            cart: cloneCart,
        })
    }
    handleTangSoLuong = (shoe) => {
        let index = this.state.cart.findIndex((item) => {
            return item.id == shoe.id;
        })
        let clonecart = [...this.state.cart];
        clonecart[index].quantity += 1;
        this.setState({
            cart: clonecart,
        })
    }
    handleGiamSoLuong = (shoe) => {
        let index = this.state.cart.findIndex((item) => {
            return item.id == shoe.id;
        })
        let clonecart = [...this.state.cart];
        clonecart[index].quantity -= 1;
        if (clonecart[index].quantity <= 0){
            clonecart.splice(index,1)
        }
        this.setState({
            cart: clonecart,
        })
    }
  render() {
    return (
      <div className='container'>
        <Cart handleGiamSoLuong = {this.handleGiamSoLuong} handleTangSoLuong = {this.handleTangSoLuong} cart = {this.state.cart}></Cart>
        <ListShoe 
        handleAddToCart = {this.handleAddToCart}
        handleChangDetail = {this.handleChangDetail} shoeArr = {this.state.shoeArr} ></ListShoe>
        <DetailShoe shoe = {this.state.detail}></DetailShoe>
      </div>
    )
  }
}
