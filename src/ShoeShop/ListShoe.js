import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    renderShoeList = () => {
        return this.props.shoeArr.map((item) => {
            return <ItemShoe handleChangDetail = {this.props.handleChangDetail}
            handleAddToCart = {this.props.handleAddToCart} data = {item}></ItemShoe>
        })
    }
  render() {
    return (
      <div className='row'>
        {this.renderShoeList()}
      </div>
    )
  }
}
