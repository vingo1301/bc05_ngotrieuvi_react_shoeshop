import React, { Component } from 'react'

export default class 
 extends Component {
  render() {
    let shoe = this.props.shoe;
    return (
      <div className='row detailShoe'>
        <div className='col-6'><img src={shoe.image} alt="" /></div>
        <div className='col-6 textDetail'>
            <h2>{shoe.name}</h2>
            <span>{shoe.price}$</span>
            <p>{shoe.description}</p>
        </div>
      </div>
    )
  }
}
